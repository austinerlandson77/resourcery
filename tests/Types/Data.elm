module Types.Data exposing
    ( allen
    , allensId
    , austin
    , austinsId
    , austinsProwess
    , budgeting
    , bus
    , cS
    , chrisId
    , codingParadigms
    , dom
    , domInfo
    , domsId
    , domsProwess
    , elm
    , emptyFellowship
    , emptyQuest
    , emptySummons
    , fellowship1
    , fellowship2
    , fifteenPointsPerJourney
    , fiftyPointQuest
    , fiftyPointSummons
    , fiftyPointSummonsId
    , fiftyPointSummonsPair
    , fiftyPointSummonsTwoFellows
    , fiftyPointSummonsTwoFellowsId
    , fiftyPointSummonsTwoFellowsPair
    , firstSummons
    , firstSummonsId
    , fourteenPointsPerJourney
    , fp
    , fpMastery
    , haskell
    , incantation1
    , incantation1Info
    , incantation2
    , incantation2Info
    , incantation3
    , incantation4
    , incantation5
    , incantation6
    , incantation7
    , incantationA
    , incantationIds
    , incantationPairs
    , incantations
    , javascript
    , jessicaId
    , jj
    , jjsId
    , joani
    , joanisId
    , joe
    , joeId
    , magic
    , marketing
    , michael
    , michaelsId
    , middleEarth
    , oop
    , patron1
    , patrons
    , php
    , python
    , quest1
    , quest2
    , ruby
    , sales
    , secondSummons
    , secondSummonsId
    , shortPast
    , shortSummonsNoFellows
    , shortSummonsNoFellowsId
    , shortSummonsNoFellowsPair
    , sourcerers
    , summons
    , summonsNoQuestOneFellow
    , summonsNoQuestOneFellowId
    , summonsNoQuestOneFellowPair
    )

import Account exposing (Person(..))
import AssocList as Dict
import AssocSet as Set exposing (Set)
import Estimate exposing (Scope(..))
import Fellow exposing (Spell(..), Why(..))
import Fuzz exposing (Fuzzer)
import Graph
import Helpers.Date exposing (cal)
import History exposing (Past)
import ID
import Incantation
    exposing
        ( Description(..)
        , Philosophy(..)
        , Proposal(..)
        , Quest
        , Reason(..)
        )
import Journey exposing (Caesura(..))
import Magic exposing (Mastery(..), Prowess)
import Name exposing (..)
import Patron
import Prediction exposing (..)
import Row
import Sourcerer
import Spell
import Summons
import Symmathesy exposing (Symmathesy)
import Time exposing (Month(..))


middleEarth : Symmathesy
middleEarth =
    { incantations = incantations
    , magic = magic
    , patrons = Dict.empty
    , summons = summons
    , sourcerers = sourcerers
    }


colorChange : Incantation.Proposal
colorChange =
    AsA patron1
        (IWantTo "change the color of the button to blue")
        (SoThat "it is consistent with the site theme.")


deployment : Incantation.Proposal
deployment =
    AsA patron1
        (IWantTo "deploy my app")
        (SoThat "I can see it somewhere")


implementSearch : Incantation.Proposal
implementSearch =
    AsA patron1
        (IWantTo "implement search")
        (SoThat "I can find what I'm looking for.")


indexPage : Incantation.Proposal
indexPage =
    AsA patron1
        (IWantTo "build an index page for Users")
        (SoThat "I can see who's using the system")


integration : Incantation.Proposal
integration =
    AsA patron1
        (IWantTo "Integrate with 3rd Party Oauth client")
        (SoThat "users can connect their profiles")


updateColor : Incantation.Proposal
updateColor =
    AsA patron1
        (IWantTo "update the color of the button")
        (SoThat "it is consistent with the theme")


sell : Incantation.Proposal
sell =
    AsA (Magic jjsId)
        (IWantTo "Bring in more clients")
        (SoThat "the business can grow")


incantationA : ( Incantation.Id, Incantation.Table )
incantationA =
    Row.new Incantation.id incantationAInfo


incantationAInfo : Incantation.Info
incantationAInfo =
    { proposal = sell
    , effort = GuessOf Five salesMastery
    }


incantations : Incantation.Table
incantations =
    incantationPairs
        |> List.map Tuple.second
        |> List.foldl Dict.union Dict.empty


incantationIds : List Incantation.Id
incantationIds =
    List.map Tuple.first incantationPairs


incantationPairs : List ( Incantation.Id, Incantation.Table )
incantationPairs =
    [ incantationA
    , incantation1
    , incantation2
    , incantation3
    , incantation4
    , incantation5
    , incantation6
    , incantation7
    ]


incantation1Info : Incantation.Info
incantation1Info =
    { proposal = colorChange
    , effort = GuessOf Thirteen (MasterOf <| Set.singleton elm)
    }


incantation1 : ( Incantation.Id, Incantation.Table )
incantation1 =
    Row.new Incantation.id incantation1Info


incantation2Info : Incantation.Info
incantation2Info =
    { proposal = indexPage
    , effort = GuessOf Thirteen (MasterOf <| Set.fromList [ php, oop ])
    }


incantation2 : ( Incantation.Id, Incantation.Table )
incantation2 =
    Row.new Incantation.id incantation2Info


incantation3 : ( Incantation.Id, Incantation.Table )
incantation3 =
    Row.new Incantation.id
        { proposal = updateColor
        , effort = GuessOf Two (MasterOf <| Set.fromList [ oop ])
        }


incantation4 : ( Incantation.Id, Incantation.Table )
incantation4 =
    Row.new Incantation.id
        { proposal = implementSearch
        , effort = GuessOf Thirteen fpMastery
        }


incantation5 : ( Incantation.Id, Incantation.Table )
incantation5 =
    Row.new Incantation.id
        { proposal = deployment
        , effort = GuessOf Thirteen fpMastery
        }


incantation6 : ( Incantation.Id, Incantation.Table )
incantation6 =
    Row.new Incantation.id
        { proposal = integration
        , effort = GuessOf Thirteen fpMastery
        }


incantation7 : ( Incantation.Id, Incantation.Table )
incantation7 =
    Row.new Incantation.id
        { proposal = colorChange
        , effort = GuessOf One fpMastery
        }


patrons : Patron.Table
patrons =
    Tuple.second joe


summons : Summons.Table
summons =
    List.foldl Dict.union Dict.empty <|
        List.map Tuple.second
            [ shortSummonsNoFellowsPair
            , secondSummons
            ]


patron1 : Person
patron1 =
    Generous joeId


joeId : Patron.Id
joeId =
    Tuple.first joe


joe : ( Patron.Id, Patron.Table )
joe =
    Row.new Patron.id { name = Name "Joe" }


jj : ( Sourcerer.Id, Sourcerer.Table )
jj =
    Row.new Sourcerer.id jjInfo


jjInfo : Sourcerer.Info
jjInfo =
    { name = Name "JJ"
    , tale = fourteenPointsPerJourney
    , skills = jjsProwess
    }


jjsId : Sourcerer.Id
jjsId =
    Tuple.first jj



-- 10.10


jjsProwess : Prowess
jjsProwess =
    Dict.fromList [ ( sales, 10 ), ( budgeting, 20 ) ]


michael : ( Sourcerer.Id, Sourcerer.Table )
michael =
    Row.new Sourcerer.id michaelInfo


michaelInfo : Sourcerer.Info
michaelInfo =
    { name = Name "Michael"
    , tale = fourteenPointsPerJourney
    , skills = michaelsProwess
    }


michaelsId : Sourcerer.Id
michaelsId =
    Tuple.first michael



-- 5


michaelsProwess : Prowess
michaelsProwess =
    Dict.fromList [ ( sales, 5 ) ]


allen : ( Sourcerer.Id, Sourcerer.Table )
allen =
    Row.new Sourcerer.id allenInfo


allenInfo : Sourcerer.Info
allenInfo =
    { name = Name "Allen"
    , tale = fourteenPointsPerJourney
    , skills = allensProwess
    }


allensId : Sourcerer.Id
allensId =
    Tuple.first allen



-- 5


allensProwess : Prowess
allensProwess =
    Dict.fromList [ ( sales, 5 ), ( marketing, 10 ) ]


joani : ( Sourcerer.Id, Sourcerer.Table )
joani =
    Row.new Sourcerer.id joaniInfo


joaniInfo : Sourcerer.Info
joaniInfo =
    { name = Name "Joani"
    , tale = fourteenPointsPerJourney
    , skills = joanisProwess
    }


joanisId : Sourcerer.Id
joanisId =
    Tuple.first joani



-- 3.05


joanisProwess : Prowess
joanisProwess =
    Dict.fromList [ ( sales, 2 ), ( marketing, 5 ), ( accounting, 10 ) ]


dom : ( Sourcerer.Id, Sourcerer.Table )
dom =
    Row.new Sourcerer.id domInfo


domInfo : Sourcerer.Info
domInfo =
    { name = Name "Dominic Serrano"
    , tale = fourteenPointsPerJourney
    , skills = domsProwess
    }


domsId : Sourcerer.Id
domsId =
    Tuple.first dom


domsProwess : Prowess
domsProwess =
    [ ( ruby, 10 ), ( php, 33 ) ]
        |> Dict.fromList


austin : ( Sourcerer.Id, Sourcerer.Table )
austin =
    Row.new Sourcerer.id
        { name = Name "Austin Erlandson"
        , tale = fifteenPointsPerJourney
        , skills = austinsProwess
        }


austinsId : Sourcerer.Id
austinsId =
    Tuple.first austin


austinsProwess : Prowess
austinsProwess =
    [ ( elm, 20 ), ( haskell, 15 ), ( ruby, 30 ) ]
        |> Dict.fromList


chris : ( Sourcerer.Id, Sourcerer.Table )
chris =
    Row.new Sourcerer.id
        { name = Name "Chris Honiball"
        , tale = twentyPointsPerJourney
        , skills = chrisProwess
        }


chrisId : Sourcerer.Id
chrisId =
    Tuple.first chris


chrisProwess : Prowess
chrisProwess =
    [ ( javascript, 40 ), ( php, 13 ) ]
        |> Dict.fromList


jessica : ( Sourcerer.Id, Sourcerer.Table )
jessica =
    Row.new Sourcerer.id
        { name = Name "Jessica Raines"
        , tale = seventeenPointsPerJourney
        , skills = jessicaProwess
        }


jessicaId : Sourcerer.Id
jessicaId =
    Tuple.first jessica


jessicaProwess : Prowess
jessicaProwess =
    [ ( cS, 40 ), ( javascript, 13 ) ]
        |> Dict.fromList


shortPast : Past
shortPast =
    [ ( One, cal 2018 Oct 22 )
    ]


fourteenPointsPerJourney : Past
fourteenPointsPerJourney =
    -- Should avg to 14 points
    [ ( One, cal 2018 Jan 1 )

    -- 15 Points
    , ( Five, cal 2018 Oct 23 )
    , ( One, cal 2018 Oct 22 )
    , ( Three, cal 2018 Oct 21 )
    , ( One, cal 2018 Oct 20 )
    , ( Five, cal 2018 Oct 19 )

    -- 15 Points
    , ( One, cal 2017 Oct 21 )
    , ( Three, cal 2017 Oct 21 )
    , ( Eight, cal 2017 Oct 21 )
    , ( Three, cal 2017 Oct 21 )
    ]


fifteenPointsPerJourney : Past
fifteenPointsPerJourney =
    -- Should avg to 15 points
    -- 15 Points
    [ ( Five, cal 2018 Oct 23 )
    , ( One, cal 2018 Oct 22 )
    , ( Three, cal 2018 Oct 21 )
    , ( One, cal 2018 Oct 20 )

    -- 15 Points
    , ( Five, cal 2018 Oct 19 )
    , ( One, cal 2017 Oct 21 )
    , ( Three, cal 2017 Oct 21 )
    , ( Eight, cal 2017 Oct 21 )
    , ( Three, cal 2017 Oct 21 )
    ]


seventeenPointsPerJourney : Past
seventeenPointsPerJourney =
    -- Should avg to 17 points
    [ ( Thirteen, cal 2018 Jan 1 )

    -- 17 Points
    , ( Five, cal 2018 Oct 23 )
    , ( Three, cal 2018 Oct 22 )
    , ( Three, cal 2018 Oct 21 )
    , ( One, cal 2018 Oct 20 )
    , ( Five, cal 2018 Oct 19 )

    -- 17 Points
    , ( One, cal 2017 Oct 21 )
    , ( One, cal 2017 Oct 21 )
    , ( Thirteen, cal 2017 Oct 21 )
    , ( Two, cal 2017 Oct 21 )
    ]


twentyPointsPerJourney : Past
twentyPointsPerJourney =
    -- Should avg to 20 points
    [ ( Thirteen, cal 2018 Jan 1 )

    -- 20 Points
    , ( Eight, cal 2018 Oct 23 )
    , ( Three, cal 2018 Oct 22 )
    , ( Three, cal 2018 Oct 21 )
    , ( One, cal 2018 Oct 20 )
    , ( Five, cal 2018 Oct 19 )

    -- 20 Points
    , ( One, cal 2017 Oct 21 )
    , ( Three, cal 2017 Oct 21 )
    , ( Thirteen, cal 2017 Oct 21 )
    , ( Three, cal 2017 Oct 21 )
    ]


fiftyPointSummonsId : Summons.Id
fiftyPointSummonsId =
    Tuple.first fiftyPointSummonsPair


fiftyPointSummons : Summons.Table
fiftyPointSummons =
    Tuple.second fiftyPointSummonsPair


fiftyPointSummonsPair : ( Summons.Id, Summons.Table )
fiftyPointSummonsPair =
    Row.new Summons.id
        { quest = fiftyPointQuest
        , fellowship = fellowship1
        , patronId = joeId
        }


fiftyPointSummonsTwoFellowsId : Summons.Id
fiftyPointSummonsTwoFellowsId =
    Tuple.first fiftyPointSummonsTwoFellowsPair


fiftyPointSummonsTwoFellows : Summons.Table
fiftyPointSummonsTwoFellows =
    Tuple.second fiftyPointSummonsTwoFellowsPair


fiftyPointSummonsTwoFellowsPair : ( Summons.Id, Summons.Table )
fiftyPointSummonsTwoFellowsPair =
    Row.new Summons.id
        { quest = fiftyPointQuest
        , fellowship = fellowship2
        , patronId = joeId
        }


emptySummons : Summons.Table
emptySummons =
    Dict.empty


shortSummonsNoFellows : Summons.Table
shortSummonsNoFellows =
    Tuple.second shortSummonsNoFellowsPair


shortSummonsNoFellowsPair : ( Summons.Id, Summons.Table )
shortSummonsNoFellowsPair =
    Row.new Summons.id
        { quest = quest1
        , fellowship = emptyFellowship
        , patronId = joeId
        }


shortSummonsNoFellowsId : Summons.Id
shortSummonsNoFellowsId =
    Tuple.first shortSummonsNoFellowsPair


summonsNoQuestOneFellow : Summons.Table
summonsNoQuestOneFellow =
    Tuple.second summonsNoQuestOneFellowPair


summonsNoQuestOneFellowPair : ( Summons.Id, Summons.Table )
summonsNoQuestOneFellowPair =
    Row.new Summons.id
        { quest = emptyQuest
        , fellowship = fellowship1
        , patronId = joeId
        }


summonsNoQuestOneFellowId : Summons.Id
summonsNoQuestOneFellowId =
    Tuple.first summonsNoQuestOneFellowPair


firstSummons : ( Summons.Id, Summons.Table )
firstSummons =
    Row.new Summons.id
        { quest = quest1
        , fellowship = fellowship1
        , patronId = joeId
        }


quest1 : Quest
quest1 =
    Set.fromList <| List.map Tuple.first [ incantation1 ]


quest2 : Quest
quest2 =
    Set.fromList <| List.map Tuple.first [ incantation2 ]


fiftyPointQuest : Quest
fiftyPointQuest =
    Set.fromList <| List.drop 2 incantationIds


emptyQuest : Quest
emptyQuest =
    Set.empty


fellowship1 : Fellow.Ship
fellowship1 =
    Dict.fromList
        [ ( austinsId
          , Dict.empty
          )
        ]


fellowship2 : Fellow.Ship
fellowship2 =
    Dict.fromList
        [ ( austinsId
          , Dict.fromList [ ( Tuple.first incantation1, Blocked <| Because "not well defined..." ) ]
          )
        , ( domsId
          , Dict.fromList [ ( Tuple.first incantation3, Casting ) ]
          )
        ]


emptyFellowship : Fellow.Ship
emptyFellowship =
    Dict.empty


firstSummonsId : Summons.Id
firstSummonsId =
    Tuple.first firstSummons


secondSummons : ( Summons.Id, Summons.Table )
secondSummons =
    Row.new Summons.id
        { quest = quest2
        , fellowship = fellowship2
        , patronId = joeId
        }


secondSummonsId : Summons.Id
secondSummonsId =
    Tuple.first secondSummons


sourcerers : Sourcerer.Table
sourcerers =
    List.foldl Dict.union Dict.empty <|
        List.map Tuple.second [ austin, dom, chris, jessica, jj, allen, michael, joani ]



{-
   Magic
-}


magic : Magic.Table
magic =
    Graph.fromNodeLabelsAndEdgePairs
        -- Business
        [ "Business"
        , "Budgeting"
        , "Sales"
        , "Marketing"
        , "Accounting"

        -- IT
        , "Technology"

        -- High Level
        , "Coding Paradigms"

        -- Paradigms
        , "Object Oriented"
        , "Hindley Milner"

        -- Languages
        , "Ruby"
        , "Python"
        , "C#"
        , "PHP"
        , "Elm"
        , "Haskell"
        , "Javascript"
        ]
        [ ( bus, budgeting )
        , ( bus, sales )
        , ( bus, marketing )
        , ( bus, accounting )
        , ( bus, technology )
        , ( technology, codingParadigms )
        , ( codingParadigms, oop )
        , ( codingParadigms, fp )
        , ( oop, ruby )
        , ( oop, python )
        , ( oop, cS )
        , ( oop, php )
        , ( oop, javascript )
        , ( fp, elm )
        , ( fp, haskell )
        ]


bus : Magic.Id
bus =
    0


budgeting : Magic.Id
budgeting =
    1


sales : Magic.Id
sales =
    2


marketing : Magic.Id
marketing =
    3


accounting : Magic.Id
accounting =
    4


technology : Magic.Id
technology =
    5


codingParadigms : Magic.Id
codingParadigms =
    6


oop : Magic.Id
oop =
    7


fp : Magic.Id
fp =
    8


ruby : Magic.Id
ruby =
    9


python : Magic.Id
python =
    10


cS : Magic.Id
cS =
    11


php : Magic.Id
php =
    12


elm : Magic.Id
elm =
    13


haskell : Magic.Id
haskell =
    14


javascript : Magic.Id
javascript =
    15



{-
   Mastery
-}


fpMastery : Mastery
fpMastery =
    MasterOf <| Set.singleton fp


salesMastery : Mastery
salesMastery =
    MasterOf <| Set.singleton sales
