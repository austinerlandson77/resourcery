module Types.Forecasting exposing (suite)

import AssocList as Dict
import AssocSet as Set exposing (Set)
import Expect exposing (FloatingPointTolerance(..))
import Fuzz exposing (Fuzzer)
import Helpers.Sum as Sum
import Incantation
import List.Extra as ListE
import Magic
import Symmathesy
import Test exposing (..)
import Types.Data exposing (..)


suite : Test
suite =
    describe "Forecasting: Who's available to work on what when?"
        [ describe "summon"
            [ test "given an empty Quest returns no sourcerers" <|
                \() ->
                    Expect.equalLists
                        (Symmathesy.summon Set.empty middleEarth)
                        []
            , test "given a Quest [ incantationA ] returns competent and available business people" <|
                \() ->
                    let
                        quest : Incantation.Quest
                        quest =
                            Set.fromList (List.map Tuple.first [ incantationA ])
                    in
                    Symmathesy.summon quest middleEarth
                        |> Expect.equalLists
                            [ ( jjsId, "53" ), ( allensId, "27" ), ( joanisId, "20" ) ]
            , test "given a Quest [ incantation1,2 ] returns competent and available developers" <|
                \() ->
                    let
                        quest : Incantation.Quest
                        quest =
                            Set.fromList (List.map Tuple.first [ incantation1, incantation2 ])
                    in
                    Symmathesy.summon quest middleEarth
                        |> Expect.equalLists
                            [ ( domsId, "37" ), ( chrisId, "33" ), ( austinsId, "30" ) ]
            , test "given sourcerers with equal prowess" <|
                \() ->
                    let
                        quest : Incantation.Quest
                        quest =
                            Set.fromList (List.map Tuple.first [ incantation3 ])
                    in
                    Symmathesy.summon quest middleEarth
                        |> Expect.equalLists
                            [ ( chrisId, "36" ), ( jessicaId, "36" ), ( domsId, "29" ) ]
            , fuzz questF "ensure +-1 from 100 percent" <|
                \quest ->
                    Symmathesy.summon quest middleEarth
                        |> Sum.list
                            (Tuple.second
                                >> String.toFloat
                                >> Maybe.withDefault (1 / 0)
                            )
                        |> Expect.within (Absolute 1) 100
            , fuzz magicF "hopsFrom 0 0" <|
                Magic.hopsFrom bus bus
                    >> Expect.equal 0
            , fuzz magicF "hopsFrom 0 1" <|
                Magic.hopsFrom bus sales
                    >> Expect.equal 1
            , fuzz magicF "hopsFrom 1 0" <|
                Magic.hopsFrom sales bus
                    >> Expect.equal 1
            , fuzz magicF "hopsFrom 1 2" <|
                Magic.hopsFrom budgeting sales
                    >> Expect.equal 2
            , fuzz magicF "hopsFrom 2 1" <|
                Magic.hopsFrom sales budgeting
                    >> Expect.equal 2
            , fuzz magicF "hopsFrom 10 14" <|
                Magic.hopsFrom ruby elm
                    >> Expect.equal 4
            , fuzz magicF "hopsFrom 14 16" <|
                Magic.hopsFrom elm javascript
                    >> Expect.equal 4
            , fuzz magicF "hopsFrom 16 14" <|
                Magic.hopsFrom javascript elm
                    >> Expect.equal 4

            -- TODO: Make this state not constructable by making the Magic.Id opaque
            -- and forcing these through a Magic.id constructor.
            , fuzz magicF "hopsFrom notInGraph 14" <|
                Magic.hopsFrom 999 elm
                    >> Expect.within (Absolute 0) (1 / 0)
            , fuzz magicF "hopsFrom 14 notInGraph" <|
                Magic.hopsFrom elm 999
                    >> Expect.within (Absolute 0) (1 / 0)
            ]
        , fuzz magicF "score" <|
            Magic.score austinsProwess (Set.singleton elm)
                >> Expect.within (Absolute 0) 25.625
        ]


questF : Fuzzer Incantation.Quest
questF =
    incantationPairs
        |> ListE.subsequences
        |> ListE.filterNot List.isEmpty
        |> List.map (makeQuest >> Fuzz.constant)
        |> Fuzz.oneOf


magicF : Fuzzer Magic.Table
magicF =
    Fuzz.constant magic


makeQuest : List ( a, b ) -> Set a
makeQuest =
    Set.fromList << List.map Tuple.first
