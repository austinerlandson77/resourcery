module Config where

import Deps
-- Minor Imports only importing certain functions
import Control.Monad.Logger (runNoLoggingT, runStdoutLoggingT)

import Data.Aeson (FromJSON)
import Data.ByteString.Char8 as Char8
import Data.Time.Clock (NominalDiffTime)
import Data.Yaml.Config (loadYamlSettings, useEnv)

data DBConfig = DBConfig
    { dbPort :: Int
    , dbHost :: String
    , dbUser :: String
    , dbPass :: String
    , dbName :: String
    }
    deriving Generic
instance FromJSON DBConfig

data ServerConfig = ServerConfig
    { env      :: Environment
    , port     :: Int
    , dbConfig :: DBConfig
    }
    deriving Generic
instance FromJSON ServerConfig

data Environment
    = Localhost
    | Production
    deriving Generic
instance FromJSON Environment

-- Utilities

type Seconds = NominalDiffTime

loadConf :: (FromJSON settings) => IO settings
loadConf = loadYamlSettings ["config/settings.yaml"] [] useEnv

mkPool :: ServerConfig -> IO ConnectionPool
mkPool ServerConfig { dbConfig = DBConfig { dbHost, dbPort, dbUser, dbPass, dbName }, env }
    = case env of
        Localhost -> runStdoutLoggingT
            (createPostgresqlPool connectionString connectionsInPool)
        Production -> runNoLoggingT
            (createPostgresqlPool connectionString connectionsInPool)
  where
    connectionsInPool = case env of
        Localhost  -> 2
        Production -> 8
    connectionString = Char8.pack $ Prelude.unwords
        [ "host=" ++ dbHost
        , "port=" ++ show dbPort
        , "user=" ++ dbUser
        , "password=" ++ dbPass
        , "dbname=" ++ dbName
        ]
