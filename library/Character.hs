{-# LANGUAGE DeriveAnyClass #-}
module Character
  ( Deity(..)
  , Human(..)
  , dbDeity
  )
where

import           Data.Morpheus.Types            ( GQLType(..) )
import           Data.Text                      ( Text )
import           GHC.Generics                   ( Generic )
import           Places  ( City(..), Realm(..) )

data Deity = Deity
  { fullName :: Text -- Non-Nullable Field
  , power    :: Maybe Text -- Nullable Field
  , realm    :: Realm
  } deriving (Generic, GQLType)

dbDeity :: Maybe Text -> Maybe Text -> IO (Either String Deity)
dbDeity _ _ = return $ Right $ Deity { fullName = "Morpheus"
                                     , power    = Just "Shapeshifting"
                                     , realm    = Dream
                                     }

data Human = Human
  { name :: Text
  , home :: City
  } deriving (Generic, GQLType)
