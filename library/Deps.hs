module Deps
    ( module Required
    , (#)
    , (...) -- Dependencies or common imports but I don't like import Imports
    , (&)
    )
where

import Control.Monad.IO.Class as Required (liftIO)
import Control.Monad.Reader as Required (ReaderT)
import Data.Morpheus as Required (interpreter)
import Data.Morpheus.Types as Required
    (GQLRootResolver(..), GQLType, lift, IORes, Undefined(..))
import Data.Text as Required (Text)
import Data.Typeable as Required (Typeable)
import Database.Persist.Postgresql as Required
import GHC.Generics as Required (Generic)
import GHC.Records as Required (HasField, getField)
import GHC.TypeLits as Required (KnownSymbol)
import Servant.Pagination as Required
import GHC.Int as Required (Int64)

-- Utils
import Data.Function ((&))
(#) :: (a -> b) -> (b -> c) -> a -> c
(#) = flip (.)

(...) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(...) = (.) . (.) -- Blackbird

infixr 9 ...
