{-# LANGUAGE AllowAmbiguousTypes  #-}
{-# LANGUAGE UndecidableInstances #-}
-- {-# LANGUAGE DuplicateRecordFields #-}

module Models where

import Deps
import Database.Persist.TH
    ( mkMigrate
    , mkPersist
    , mpsPrefixFields
    -- , mpsBackend
    -- , mkPersistSettings
    , persistLowerCase
    , share
    , sqlSettings
    )

-- Amplify scaffold
-- type Blog @model {
--   id: ID!
--   name: String!
--   posts: [Post] @connection(name: "BlogPosts")
-- }
-- type Post @model {
--   id: ID!
--   title: String!
--   blog: Blog @connection(name: "BlogPosts")
--   comments: [Comment] @connection(name: "PostComments")
-- }
-- type Comment @model {
--   id: ID!
--   content: String
--   post: Post @connection(name: "PostComments")
-- }


share
    [ mkPersist $ sqlSettings { mpsPrefixFields = False }
    , mkMigrate "migrateAll"
    ] [persistLowerCase|
Account json
    name Text
    email Text
    password Text
    deriving Eq Generic Show

Todo json
    account_id AccountId
    description Text
    deriving Eq Generic Show
|]
instance GQLType Account
instance GQLType Todo
