{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeSynonymInstances #-}

module API where

import Deps
-- Major imports exposing everything
import Servant
import Servant.API.Generic
import Servant.Server.Generic

-- Minor Imports only importing certain functions
import Control.Monad ((<=<))
-- import Control.Monad.Extra (bind)
import Control.Monad.Reader (ReaderT)
-- import Control.Monad.Trans (liftIO)
import Data.Maybe (fromMaybe, maybeToList)
import Data.Text (Text)

-- persistent-postgres re-exports
--  Database.Persist.Sql which re-exports
--    Database.Persist which re-exports
--      Database.Persist.[Class,Types]
import Database.Persist.Postgresql
    ( ConnectionPool
    , PersistEntityBackend
    , SelectOpt(..)
    , SqlBackend
    , delete
    , getEntity
    , insert_
    , repsert
    , runSqlPool
    , selectList
    )
import Models

-- Not sure whether to separate this into endpoints on the collection vs instance or by Read vs Modify...
-- Can't Create, Update, and Delete all be modelled via Upsert?
--
-- data Event record
--     = Upserted record
--
-- data Event record
--     = Created record
--     | Updated record
--     | Deleted record

-- Or maybe just implement PATCH/POST/PUT/DELETE via upsert_ ???
-- data Command record path = Command
--     { upsert_ :: path
--          :- ReqBody '[JSON] record
--          :> Put/Patch '[JSON] (Entity record)
--     }
data Command record path = Command
    { create_ :: path
        :- ReqBody '[JSON] record
        :> PostNoContent '[JSON] NoContent
    , update_ :: path
        :- Capture "id" (Key record)
        :> ReqBody '[JSON] record
        :> PutNoContent '[JSON] NoContent
    , delete_ :: path
        :- Capture "id" (Key record)
        :> DeleteNoContent '[JSON] NoContent
    } deriving (Generic)

-- Types get in the way, repetition isn't great.
-- /authors/1/comments

-- Flat resources with query params?
-- /comments?id=1&name=aus

-- Many To Many relationships implicate
-- /like?preload[]=user&preload[]=comment
-- like.user.name
-- like.comment.description

-- /users?

data Query record field path = Query
    { list_ :: path
        :- Header "Range" (Ranges '[field] (Entity record))
        :> Get '[JSON] (Headers (PageHeaders '[field] (Entity record)) [Entity record])
    , show_ :: path
        :- Capture "id" (Key record)
        :> Get '[JSON] (Entity record)
    } deriving (Generic)


data Api path = Api
    {
      accounts :: path
        :- "accounts"
        :> ToServantApi (Query Account "name")
    , accountCmds :: path
        :- "accounts"
        :> ToServantApi (Command Account)
    -- Todo belongsTo account
    , todos :: path
        :- "accounts"
        :> Capture "id" (Key Account)
        :> ToServantApi (Query Todo "description")
    , todoCmds :: path
        :- "todos"
        :> ToServantApi (Command Todo)
    } deriving (Generic)

api :: ConnectionPool -> Api AsServer
api pool =
    let
        query :: forall record field
               . ( HasField field record Text
                 , KnownSymbol field
                 , PersistEntity record
                 , PersistEntityBackend record ~ SqlBackend
                 )
              => EntityField record Text
              -> (Query record field) AsServer
        query entityField =
            let
                list :: Maybe (Ranges '[field] (Entity record))
                     -> Handler
                            ( Headers
                                  (PageHeaders '[field] (Entity record))
                                  [Entity record]
                            )
                list ranges = do
                    let range :: Range field (RangeType (Entity record) field)
                        range@Range { rangeLimit, rangeOffset, rangeOrder, rangeValue }
                            = fromMaybe d'fault (extractRange =<< ranges)
                          where
                            d'fault = getDefaultRange $ Proxy @(Entity record)

                        filters :: [Filter record]
                        filters = fmap
                        -- Given an actual Pagination requirement swap for CreatedAt something...
                            (contains entityField)
                            (maybeToList rangeValue)

                        sorts :: [SelectOpt record]
                        sorts =
                            [ case rangeOrder of
                                RangeAsc  -> Asc persistIdField
                                RangeDesc -> Desc persistIdField
                            , OffsetBy rangeOffset
                            , LimitTo rangeLimit
                            ]

                    returnRange range =<< db (selectList filters sorts)
            in Query
                { list_ = list -- see list below
                , show_ = maybe (throwError err404) pure <=< db . getEntity
                }

        cmd :: forall record
             . ( PersistEntity record
               , PersistEntityBackend record ~ SqlBackend
               )
            => (Command record) AsServer
        cmd = Command
            { create_ = (NoContent <$) . db . insert_
            , update_ = (NoContent <$) ... db ... repsert -- Is there a repsert with a Maybe (Key entity) ?
            , delete_ = (NoContent <$) . db . delete
            }

        db :: ReaderT SqlBackend IO record -> Handler record
        db = liftIO . (`runSqlPool` pool)
    in Api
        { accounts    = query AccountName & genericServer
        , accountCmds = cmd & genericServer
        , todos       = const $ (query TodoDescription) & genericServer
        , todoCmds    = cmd & genericServer
        }

contains :: EntityField record Text -> Text -> Filter record
contains field val = Filter
    field
    (Left $ Deps.concat ["%", val, "%"])
    (BackendSpecificFilter "ILIKE")
