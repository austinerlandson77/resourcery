module Magic exposing
    ( Id
    , Mastery(..)
    , Prowess
    , Spell
    , Table
    , Weight
    , hopsFrom
    , learn
    , score
    )

import AssocList as Dict exposing (Dict)
import AssocSet as Set exposing (Set)
import Graph exposing (BfsNodeVisitor, Graph, NeighborSelector, NodeContext)
import Helpers.Sum as Sum
import IntDict
import Maybe.Extra as MaybE
import Prediction exposing (Prediction)



-- To work with elm-community/graph


type alias Table =
    -- FIXME:
    -- type Magic
    --     = Magic Id Info
    Graph Spell ()


type alias Spell =
    String


type alias Id =
    Graph.NodeId



-- Sourcerers gain prowess from their Mastery of skills required to perform spells.


type alias Prowess =
    Dict Id Weight


type alias Weight =
    Int


type Mastery
    = MasterOf (Set Id)


learn : Prowess -> Mastery -> Prediction -> Table -> Prowess
learn prowess (MasterOf mastery) complexity magic =
    Dict.merge
        -- Key exists in Prowess: do nothing
        Dict.insert
        -- Key exists in both: increment the experience of the prowess
        (\id p e ->
            Dict.insert id (p + e)
        )
        -- Key exists in Mastery: add experience to prowess
        (\id e ->
            Dict.insert id (e // (magic |> childrenOf id))
        )
        prowess
        (skillSetFrom complexity mastery)
        Dict.empty


childrenOf : Id -> Table -> Int
childrenOf id =
    Graph.guidedBfs
        Graph.alongOutgoingEdges
        counter
        [ id ]
        1
        >> Tuple.first


counter : BfsNodeVisitor n e Int
counter breadcrumbs pathLength acc =
    acc
        + (List.head breadcrumbs
            |> Maybe.map (\ctx -> IntDict.size ctx.outgoing)
            |> Maybe.withDefault 1
          )


skillSetFrom : Prediction -> Set Id -> Dict Id Int
skillSetFrom complexity =
    Set.map (weight complexity)
        >> Set.toList
        >> Dict.fromList


weight : Prediction -> Id -> ( Id, Int )
weight complexity skill =
    ( skill, Prediction.assess complexity )


score : Prowess -> Set Id -> Table -> Float
score =
    Sum.dictWithSet
        (\magic experience skillTo requestIn ->
            toFloat experience / 2 ^ hopsFrom skillTo requestIn magic
        )


hopsFrom : Id -> Id -> Table -> Float
hopsFrom start end =
    let
        alongAnyPath : NeighborSelector n e
        alongAnyPath { incoming, outgoing } =
            List.concatMap IntDict.keys [ incoming, outgoing ]

        lookFor : Id -> BfsNodeVisitor n e (Maybe Float)
        lookFor pathEnd breadcrumbs pathLength =
            List.head breadcrumbs
                -- Check if end -> Just NodeContext or Nothing
                |> MaybE.filter (\ctx -> ctx.node.id == pathEnd)
                -- If Just NodeContext then end found -> return Just pathLength
                |> Maybe.map (toFloat pathLength |> always)
                -- if Just pathLength keep around for remaining iterations.
                |> MaybE.or
    in
    -- Using bfs to find shortest path between requestedSkill and required
    -- NOTE: bfs is equivalent to dijkstra's when the weights of the edges are 1
    Graph.guidedBfs
        alongAnyPath
        (lookFor start)
        [ end ]
        Nothing
        >> Tuple.first
        >> Maybe.withDefault (1 / 0)
