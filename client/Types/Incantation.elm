module Incantation exposing
    ( Description(..)
    , Id(..)
    , Info
    , Philosophy(..)
    , Proposal(..)
    , Quest
    , Reason(..)
    , Table
    , id
    , value
    )

import Account exposing (Person(..))
import AssocList as Dict exposing (Dict)
import AssocSet as Set exposing (Set)
import ID exposing (ID)
import Magic exposing (Mastery)
import Prediction exposing (Prediction)


type Id
    = Id ID


type alias Table =
    Dict Id Info


type alias Info =
    -- Synonyms: Charm, Curse, Jinx
    { proposal : Proposal
    , effort : Philosophy
    }


type Proposal
    = AsA Person Description Reason


type Description
    = IWantTo String


type Reason
    = SoThat String


type Philosophy
    = GuessOf Prediction Mastery


type alias Quest =
    Set Id


value : Id -> String
value (Id v) =
    ID.value v


id : Info -> Id
id { proposal, effort } =
    let
        (AsA person (IWantTo description) (SoThat reason)) =
            proposal
    in
    Id <| ID.new <| description ++ reason
