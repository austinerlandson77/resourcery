module Patron exposing
    ( Id(..)
    , Info
    , Table
    , id
    )

import AssocList as Dict exposing (Dict)
import ID exposing (ID)
import Name exposing (Name(..))



-- Should be type Id = Id ID
-- once we get comparables for any type
-- this can be updated.


type Id
    = Id ID


type alias Table =
    Dict Id Info


type alias Info =
    { name : Name
    }


id : Info -> Id
id { name } =
    let
        (Name str) =
            name
    in
    Id <| ID.new str
