module Estimate exposing (Scope(..), in_)

import AssocList as Dict exposing (Dict)
import AssocSet as Set exposing (Set)
import Fellow
import Helpers.Sum as Sum
import ID exposing (ID)
import Incantation exposing (Philosophy(..), Quest)
import Journey exposing (Caesura(..))
import Prediction
import Result exposing (Result)
import Result.Extra as ResultE
import Sourcerer
import Summons
import Symmathesy exposing (Symmathesy)



-- Like Miles Per Hour... but instead we use Points per Journey?


type Scope
    = Approximately Int Caesura


in_ : Caesura -> Summons.Id -> Symmathesy -> Scope
in_ time str world =
    let
        { summons } =
            world

        result : Result String Summons.Info
        result =
            Summons.get str summons
    in
    ResultE.unpack
        (\_ -> Approximately 0 time)
        (withSummons time world)
        result


withSummons : Caesura -> Symmathesy -> Summons.Info -> Scope
withSummons time { incantations, sourcerers } summons =
    let
        { quest, fellowship } =
            summons

        int =
            approximation fellowship quest sourcerers incantations
    in
    case time of
        Hours ->
            Approximately int Hours

        Days ->
            Approximately (roundDays int) Days

        Weeks ->
            Approximately (roundWeeks int) Weeks


roundDays : Int -> Int
roundDays int =
    round <| toFloat int / 8


roundWeeks : Int -> Int
roundWeeks int =
    round <| toFloat int / 40


cost : Incantation.Quest -> Incantation.Table -> Int
cost quest incantations =
    Sum.list
        (.effort
            >> (\(GuessOf prediction _) -> prediction)
            >> Prediction.assess
        )
    <|
        (incantations |> findIncantations quest)


approximation :
    Fellow.Ship
    -> Quest
    -> Sourcerer.Table
    -> Incantation.Table
    -> Int -- Hours
approximation fellowship quest sourcerers incantations =
    round <| (*) (toFloat (cost quest incantations) / (toFloat <| Sum.list (.tale >> Sourcerer.velocity) <| (sourcerers |> findFellows fellowship))) (toFloat <| Journey.hours)


findIncantations : Set k -> Dict k v -> List v
findIncantations set =
    let
        member : k -> v -> Bool
        member id _ =
            Set.member id set
    in
    Dict.filter member >> Dict.values


findFellows : Fellow.Ship -> Sourcerer.Table -> List Sourcerer.Info
findFellows fellowship sourcerers =
    Dict.merge
        -- Key in first
        (\_ _ result -> result)
        -- Key in both
        (\key _ sourcerer result ->
            Dict.insert key sourcerer result
        )
        -- Key in second
        (\_ _ result -> result)
        fellowship
        sourcerers
        Dict.empty
        |> Dict.values
