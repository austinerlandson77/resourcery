module Journey exposing (Caesura(..), days, hours)


type
    -- Interval conflicts with Time.Extra
    -- Caesura: noun. interruption
    Caesura
    = Weeks
    | Days
    | Hours


hours : Int
hours =
    80


days : Int
days =
    14
