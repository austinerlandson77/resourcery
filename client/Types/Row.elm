module Row exposing (new)

import AssocList as Dict exposing (Dict)

new :
    -- Type.key_function for creating a UUID
    -- from properties in the Info of that type.
    (value -> k)
    -> value -- Type.Info
    -> ( k, Dict k value )
new key value =
    let
        id =
            key value

        table =
            Dict.fromList [ ( id, value ) ]
    in
    ( id, table )
