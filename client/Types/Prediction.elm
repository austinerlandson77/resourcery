module Prediction exposing
    ( Prediction(..)
    , assess
    )

-- TODO: Add a Kreplit type.
-- Kreplit: noun. A unit which is the product of time AND attention
-- type Kreplit = Krplt Milliseconds Focus


type Prediction
    = One
    | Two
    | Three
    | Five
    | Eight
    | Thirteen


assess : Prediction -> Int
assess prediction =
    case prediction of
        One ->
            1

        Two ->
            2

        Three ->
            3

        Five ->
            5

        Eight ->
            8

        Thirteen ->
            13
