module Sourcerer exposing
    ( Id
    , Info
    ,  Table
       -- , gen

    , id
    , value
    , velocity
    )

import AssocList as Dict exposing (Dict)
import Helpers.Sum as Sum
import History exposing (..)
import ID exposing (ID)
import Journey
import List.Extra as ListE
import Magic exposing (Mastery(..), Prowess)
import Name exposing (..)
import Prediction
import Random exposing (Generator)
import Set exposing (Set)
import Time exposing (utc)
import Time.Extra as TimE exposing (Interval(..))


type Id
    = Id ID


type alias Table =
    Dict Id Info


type alias Info =
    { name : Name
    , tale : Past -- List of Deeds / Incantations performed
    , skills : Prowess -- Seed of Magic + prowess attained from philosophies learned by completing Incantations on Quests...
    }


velocity : Past -> Int
velocity tale =
    let
        deedInfo : ( Deed, Past ) -> ( Int, Int )
        deedInfo ( d, ds ) =
            let
                dds =
                    d :: ds

                countOfDeeds =
                    List.length dds
            in
            -- Weighting
            ( Sum.list (History.prediction >> Prediction.assess) dds * countOfDeeds
            , countOfDeeds
            )

        inscribed : List ( Int, Int )
        inscribed =
            List.sortBy (\( _, timestamp ) -> Time.posixToMillis timestamp) tale
                |> ListE.groupWhile
                    (\( _, timeA ) ( _, timeB ) ->
                        TimE.diff Day utc timeA timeB <= Journey.days
                    )
                |> List.map deedInfo

        sumOfAssessments =
            toFloat <| Sum.list Tuple.first inscribed

        sumOfCountsOfDeeds =
            toFloat <| Sum.list Tuple.second inscribed
    in
    -- I need round to be the behavior of Integer division not truncate.
    round (sumOfAssessments / sumOfCountsOfDeeds)


value : Id -> String
value (Id v) =
    ID.value v


id : Info -> Id
id { name } =
    let
        (Name str) =
            name
    in
    Id <| ID.new str
