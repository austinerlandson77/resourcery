module Summons exposing
    ( Id
    , Info
    , Table
    , get
    , id
    )

import AssocList as Dict exposing (Dict)
import AssocSet as Set exposing (Set)
import Fellow
import ID exposing (ID)
import Incantation exposing (Quest)
import Patron
import Result exposing (Result)
import Sourcerer


type Id
    = Id ID


type alias Table =
    Dict Id Info


type alias Info =
    { quest : Quest
    , fellowship : Fellow.Ship
    , patronId : Patron.Id
    }


id : Info -> Id
id { fellowship, patronId, quest } =
    let
        (Patron.Id patronsId) =
            patronId

        key =
            -- A reasonably uniquely identifiable
            -- string that will get hashed further
            -- to create the next id.
            ID.value patronsId
                ++ Set.foldl ((++) << Incantation.value) "" quest
                ++ List.foldl ((++) << Sourcerer.value) "" (Dict.keys fellowship)
    in
    Id <| ID.new key


value : Id -> String
value (Id v) =
    ID.value v


get : Id -> Table -> Result String Info
get key =
    Result.fromMaybe
        ("Couldn't find Summons.Id: "
            ++ value key
            ++ " in Summons.Table"
        )
        << Dict.get key
