module RouteT exposing (Data, Event(..))

import Page exposing (Page)
import Browser exposing (UrlRequest)
import Browser.Navigation exposing (Key)
import Url exposing (Url)


type Event
    = To UrlRequest
    | From Url
    | Go Page


type alias Data =
    { key : Key
    , page : Page
    }
