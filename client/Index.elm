module Index exposing (main)

import Assets.Logo as Logo
import Auth
import AuthT
import Browser exposing (Document)
import Browser.Navigation as Nav exposing (Key)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Header
import Html exposing (Html)
import Html.Events exposing (onClick)
import Main
import MainT
import Page exposing (Gate(..))
import IndexT exposing (..)
import Port
import Return exposing (Return, ReturnF)
import Route
import RouteT
import Style exposing (edges)
import Url exposing (Url)
import Url.Parser exposing (parse)



-- MAIN


main : Program () Data Event
main =
    Browser.application
        { init = init
        , update = update
        , view = primary
        , subscriptions = subscriptions
        , onUrlChange = Route << RouteT.From
        , onUrlRequest = Route << RouteT.To
        }



primary : Data -> Document Event
primary data =
    { title = "ReSourcery"
    , body =
        [ Element.layout
            [ Font.family
                [ Font.typeface "Cormorant"
                , Font.serif
                ]
            , Font.color Style.white
            , Background.color Style.black
            ]
            (column
                (Style.area fill)
                [ Header.view
                , let
                    { route, auth } =
                        data
                  in
                  case route.page of
                    Page.Auth page ->
                        Element.map Auth <| Auth.view page auth

                    Page.Main ->
                        Element.map Route <|
                            Input.button [ Element.centerX, Element.centerY ]
                                { onPress = Just <| RouteT.Go <| Page.Auth Welcome
                                , label = text "Welcome"
                                }
                ]
            )
        ]
    }


init : flags -> Url -> Key -> Return Event Data
init _ url key =
    { route = Route.init url key |> Tuple.first
    , auth = Auth.init
    , mainD = Main.init
    }
        |> Return.singleton


update : Event -> Data -> Return Event Data
update event ({ route, mainD, auth } as data) =
    case event of
        Route action ->
            (Route.update action route)
            |> Return.map (\d -> { data | route = d })

        Main action ->
            Main.update action mainD
            |> Return.mapBoth Main (\d -> { data | mainD = d })

        Auth action ->
            Auth.update route.key action auth
            |> Return.map (\d -> { data | auth = d })


subscriptions : Data -> Sub Event
subscriptions data =
    case data.route.page of
        Page.Auth _ ->
            Auth.subscriptions data.auth.status
                |> Sub.map Auth

        Page.Main ->
            Sub.none
