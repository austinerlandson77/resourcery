module Helpers.Round exposing (commercial)

import Round exposing (roundCom)



{-
   Rounds the commercial way?
   -0.5 -> -1 instead of rounding towards positive
   apparently normal rounding would be -0.5 -> 0
-}


commercial : Int -> Float -> String
commercial =
    roundCom
