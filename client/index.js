import Amplify, { Auth } from 'aws-amplify'
import aws from './aws-exports'
import { register } from './serviceWorker'
import { Elm } from './Index'
const { Index: { init } } = Elm

Amplify.configure(aws)

const {
  ports: {
    elmToJs: { subscribe },
    jsToElm: { send }
  }
} = init()

subscribe(
  ({func, args}) =>
    Auth[func](...args)
      .then(send, send)
)


register()
