module AuthT exposing (..)

import Json.Decode as Dec

type Event
    = Changed Cred
    | Signed Method
    | Then Dec.Value


type Method
    = In
    | Up
    | Out


type Cred
    = Password String
    | Username String



type alias Data =
    { username : String
    , password : String
    , status : Request
    }

type Request
    = Dormant
    | Loading
    | Recieved Dec.Value


type Session
    = Token String
