module Route exposing
    ( init
    , goto
    , update
    )

import Browser exposing (UrlRequest(..))
import Browser.Navigation as Nav exposing (Key)
import Page exposing (Gate(..), Page)
import Return exposing (Return, return)
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s, top)
import IndexT
import RouteT exposing (..)


init : Url -> Key -> Return IndexT.Event Data
init url key =
    parse list url
        |> Maybe.withDefault (Page.Auth Welcome)
        |> Data key
        |> Return.singleton


update : Event -> Data -> Return IndexT.Event Data
update event data =
    case event of
        From url ->
            { data | page = Maybe.withDefault (Page.Auth Welcome) <| parse list url }
                |> Return.singleton

        To url ->
            (data |> Return.singleton)
                |> Return.command
                    (case url of
                        Internal location ->
                            Url.toString location
                                |> Nav.pushUrl data.key

                        External location ->
                            Nav.load location
                    )

        Go page ->
            page
                |> path
                |> Nav.pushUrl data.key
                |> return data

goto : Page -> Key -> Cmd IndexT.Event
goto page key =
    path page |> Nav.pushUrl key

-- Private

path : Page -> String
path page =
    case page of
        Page.Main ->
            "/"

        Page.Auth Register ->
            "/register"

        Page.Auth Welcome ->
            "/welcome"


list : Parser (Page -> a) a
list =
    oneOf
        [ map Page.Main top
        , map (Page.Auth Register) (s "register")
        , map (Page.Auth Welcome) (s "welcome")
        ]
