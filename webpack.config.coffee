path = require 'path'
webpack = require 'webpack'
merge = require 'webpack-merge'
# elmMinify = require 'elm-minify'
HtmlWebpackPlugin = require 'html-webpack-plugin'
CopyWebpackPlugin = require 'copy-webpack-plugin'
CleanWebpackPlugin = require 'clean-webpack-plugin'
MiniCssExtractPlugin = require 'mini-css-extract-plugin'
# # to extract the css as a separate file

# determine build env
MODE =
  if process.env.npm_lifecycle_event is 'build'
    'production'
  else
    'development'
withDebug = !process.env['npm_config_nodebug']
console.log '\x1b[36m%s\x1b[0m', "ReSourcery: mode \"#{MODE}\", withDebug: #{withDebug}\n"

console.log 'WEBPACKing with ENV:', MODE

excludes = [
  /elm-stuff/
  /node_modules/
]
# common webpack config
common =
  mode: MODE
  entry: './client/index.js'

  output:
    filename: if MODE is 'production'
        '[name]-[hash].js'
      else
        '[name].js'

    path: path.resolve __dirname, 'public'
    publicPath: '/'

  plugins: [
    new HtmlWebpackPlugin
      template: 'client/index.html'
      inject: 'body'
  ]
  resolve:
    modules: [
      path.join __dirname, 'src'
      'node_modules'
    ]
    extensions: [
      '.js'
      '.elm'
      '.css'
      '.png'
    ]

  module: rules: [
    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/
    exclude: excludes
    loader: 'url-loader'
    options:
      limit: 10000
      mimetype: 'application/font-woff'
  ,
    test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/
    exclude: excludes
    loader: 'file-loader'
  ,
    test: /\.(jpe?g|png|gif|svg)$/i
    exclude: excludes
    loader: 'file-loader'
  ,
    test: /\.css$/
    use: [
      MiniCssExtractPlugin.loader
      'css-loader'
    ]
  ]

  plugins: [
    new MiniCssExtractPlugin
      filename: 'style.css'
    new HtmlWebpackPlugin
      template: 'client/index.html'

    new CopyWebpackPlugin [
      # from: 'client/Assets/images/'
      # to:   'images/'
    # ,
      # from: 'client/Assets/icons'
      # flatten: true
    # ,
    #   from: 'client/Assets/data'
    #   flatten: true
    , from: 'client/Assets/CNAME'
    , from: 'client/Assets/manifest.json'
    # , from: 'client/Assets/browserconfig.xml'
    ]
  ]

# additional webpack settings for local env (when invoked by 'npm start')
if MODE is 'development'
  module.exports = merge common,
    devtool: 'inline-source-map'
    devServer:
      contentBase: path.join __dirname, 'client/assets'

      # Required for usage behind a proxy, see https://github.com/webpack/webpack-dev-server/releases/tag/v2.4.3
      disableHostCheck: true

      # serve index.html in place of 404 responses
      historyApiFallback: true

      # For docker-compose
      # host: '0.0.0.0'
      hot: true
      inline: true
      port: 7777
      stats: 'errors-only'
        # hash: false
        # version: false
        # timings: false
        # assets: false
        # chunks: false
        # modules: false
        # reasons: false
        # children: false
        # source: false
        # errors: false
        # errorDetails: false
        # warnings: false
        # publicPath: false

    module: rules: [
      test: /\.elm$/
      exclude: excludes
      use: [
        loader: 'elm-hot-webpack-loader'
      ,
        loader: 'elm-webpack-loader'
        options:
          # Causes issues with certain dependencies
          # See: https://github.com/elm/compiler/issues/1851
          # See: https://github.com/elm/browser/issues/10
          debug: withDebug

          verbose: withDebug
          forceWatch: true
      ]
    ]

  plugins: [
    new webpack.NamedModulesPlugin
    new webpack.NoEmitOnErrorsPlugin
  ]

# additional webpack settings for prod env (when invoked via 'npm run build')
if MODE is 'production'
  module.exports = merge common,
    module:
      rules: [
        test:    /\.elm$/
        exclude: excludes
        use:
          loader: 'elm-webpack-loader'
          options: optimize: true
      ]

    plugins: [
      # new (elmMinify.WebpackPlugin)
      # new webpack.optimize.OccurrenceOrderPlugin()

      new CleanWebpackPlugin
        root: __dirname
        exclude: []
        verbose: true
        dry: false
    ]
